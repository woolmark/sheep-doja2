/**
 *
 * スプライトを管理するクラス<br>
 *
 *
 * @author Nobu Kobayashi
 * @version 0.8
 * @see Sprite
 *
 */
public class Sprite {
	/**
	* 現在のトータルのスプライト枚数。
	* これが、256を越えるとエラーですよ。
	*/
	public static int totalIndex;
	
	/**
	* スプライトが始まる場所を示す
	*/
	public int index;
	
	/**
	* スプライトの横枚数
	*/
	public int tileX;
	
	/**
	* スプライトの縦枚数
	*/
	public int tileY;
	
	/**
	* 描かれるx座標
	*/
	public int x;
	
	/**
	* 描かれるy座標
	*/
	public int y;
	
	/**
	* 画像を反転させる
	*/
	public int turn;
	
	/**
	* スプライトの横幅
	*/
	public int width;
	
	/**
	* スプライトの縦幅
	*/
	public int height;
	
	
	/**
	* 現在のトータルのスプライト枚数を返す
	* @return - トータルのスプライト枚数
	*/
	public int getTotalIndex(){
		return totalIndex;
	}
	
	
	/**
	* スプライトの横幅を返す
	* @return - 横幅
	*/
	public int getWidth(){
		return width;
	}
	
	
	/**
	* スプライトの縦幅を返す
	* @return - 縦幅
	*/
	public int getHeight(){
		return height;
	}  
	
	
	/**
	* このスプライトクラスをコピーします
	* @return - スプライト
	*/
	public Sprite clone() {
		Sprite newSprite;
		
		newSprite = new Sprite();
		
		newSprite.index = index;
		newSprite.tileX = tileX;
		newSprite.tileY = tileY;
		newSprite.width = width;
		newSprite.height = height;
		
		return newSprite;
	}
	
}
