import java.util.Random;

import javax.microedition.lcdui.*;
import javax.microedition.rms.*;

/**
 * Sheep Canvas.
 *
 * @author takkie
 */
public class SheepCanvas extends VSCanvas implements Runnable {

  /**
  * the "0" button.
  */
  public static final int KEY_0 = 0;

  /**
  * the "1" button.
  */
  public static final int KEY_1 = 1;

  /**
  * the "2" button.
  */
  public static final int KEY_2 = 2;

  /**
  * the "3" button.
  */
  public static final int KEY_3 = 3;

  /**
  * the "4" button.
  */
  public static final int KEY_4 = 4;

  /**
  * the "5" button.
  */
  public static final int KEY_5 = 5;

  /**
  * the "6" button.
  */
  public static final int KEY_6 = 6;

  /**
  * the "7" button.
  */
  public static final int KEY_7 = 7;

  /**
  * the "8" button.
  */
  public static final int KEY_8 = 8;

  /**
  * the "9" button.
  */
  public static final int KEY_9 = 9;

  /**
  * the "*" button.
  */
  public static final int KEY_00 = 10;

  /**
  * the "#" button.
  */
  public static final int KEY_01 = 11;

  /**
  * the "UP" button.
  */
  public static final int KEY_UP = 13;

  /**
  * the "RIGHT" button.
  */
  public static final int KEY_RIGHT = 14;

  /**
  * the "DOWN" button.
  */
  public static final int KEY_DOWN = 15;

  /**
  * the "LEFT" button.
  */
  public static final int KEY_LEFT = 16;

  /**
  * the "SELECT" button.
  */
  public static final int KEY_SELECT = 17;

  /**
  *  the button.
  */
  public static final int KEY_SOFT1 = 18;

  /**
  *  the button.
  */
  public static final int KEY_SOFT2= 19;

  private static Sheep midlet;

  /*
  *
  */
  private static int i, j, k;

  /*
  *
  */
  private static int l, m, n;

    public int height = 0;
    public int width = 0;

  /**
  * Thread.
  */
  private Thread runner;
  
  /**
  * create random number.
  */
  private static Random rand;

  /**
  * the application state.
  */
  private static int state;

  /**
  * the sheep action.
  */
  private static int action = 0;

  /**
  * the sheep number.
  */
  private static int sheep_number;

  /**
  * the sheep image.
  */
  private Sprite sheep_sprite[];

  /**
  * the sheep position.
  */
  // private static int sheep_pos[][] = new int[100][3];
  private static int sheep_pos[][] = new int[30][3];

  /**
  * key flag.
  */
  private static int key_flag;

  private static long startTime = (long)0;

  private static int draw_time = 0;

  private static int frame_number = 0;

  /**
  * do nothing.
  */
  public SheepCanvas(int pattern, int palette, int width, int height) {
    super(pattern, palette, width, height);

    this.height = height;
    this.width = width;
  }

  public static void setMIDlet(Sheep mid) {
    midlet = mid;
  }

  /**
  * start application.
  */
  public void start() {
      
    /* create object */
    state = 1;
    rand = new Random();

    try {
      sheep_sprite = setSprite("/sheep.spr");
      setPalette("/palette");
    } catch (Exception e) {
	midlet.destroyApp(true);
    }

    state = 3;

    /* start running */
    runner = new Thread(this);
    runner.start();

  }

  /**
  * run method.
  */
  public void run() {

    while(true) {

      if(state == 3) {

        repaint();
        Thread.yield();
        copyArea(0, 0, width, height, 0, 0);
        drawFrameBuffer(0, 0);

        try {
          Thread.sleep(80);
        } catch (Exception e) {}

        if(isKeyPressed(KEY_SELECT) || isKeyPressed(KEY_1)) {
          state = 5;

          repaint();
          Thread.yield();
          copyArea(0, 0, width, height, 0, 0);
          drawFrameBuffer(0, 0);

          try {
            Thread.sleep(80);
          } catch (Exception e) {}

          init();
          draw_time = Sheep.TIME;

        }

      }

      else if(state == 5) {

        if((System.currentTimeMillis() - startTime) > draw_time) {
          state = 3;
        }

        runSheep();

        draw();

      }

    }

  }

  private void init() {

    draw_time = 0;
    sheep_number = 0;
    frame_number = 0;

    for(i = 0; i < sheep_pos.length; i++) {
        sheep_pos[i][0] = -sheep_sprite[0].getWidth() + (rand.nextInt() >>> 1) % (getWidth() + sheep_sprite[0].getWidth());
        sheep_pos[i][1] = (getHeight() - 40) + rand.nextInt() % 30;
        sheep_pos[i][2] = 0;
    }

    startTime = System.currentTimeMillis();

  }

  private void runSheep() {

      /* sheep action */
      action = 1 - action;

      /* run the sheep */
      for(i = 0; i < sheep_pos.length; i++) {
        if(sheep_pos[i][1] >= 0) {

          /* remove a frameouted sheep */
          if((sheep_pos[i][0] -= 5) < -sheep_sprite[0].getWidth()) {
              sheep_pos[i][0] = getWidth();
              sheep_pos[i][1] = (getHeight() - 40) + rand.nextInt() % 30;
              sheep_pos[i][2] = 0;
          }

          /* run */
          if(sheep_pos[i][0] > 50 && sheep_pos[i][0] <= 70) {
            sheep_pos[i][1] -= 3;
          }
          
          /* jump */
          else {
            if(sheep_pos[i][0] > 30 && sheep_pos[i][0] <= 50) {
                sheep_pos[i][1] += 3;
            }
            if(sheep_pos[i][0] < 60 && sheep_pos[i][2] == 0) {
                sheep_number++;
                sheep_pos[i][2] = 1;
            }
          }
        }

      }

  }

  public void draw() {

    copyArea(0, 0, width, height, 0, 0);

    drawSprite(sheep_sprite[0], 35, getHeight() - 80);
    for(l = 0; l < sheep_pos.length; l++) {
        if(sheep_pos[l][1] >= 0) {
          drawSprite(sheep_sprite[action + 1], sheep_pos[l][0], sheep_pos[l][1]);
        }
    }

    frame_number++;

    writeAllSprite();
    drawFrameBuffer(0, 0);

  }

  public void paint(Graphics g) {

      if(state == 6) {
          g.setColor(0x00FFFFFF);
          g.fillRect(0, 0, getWidth(), getHeight());
      }

      if(state == 5) {

          g.setColor(100, 255, 100);
          g.fillRect(0, 0, getWidth(), getHeight());
          g.setColor(150, 150, 255);
          g.fillRect(0, 0, getWidth(), getHeight() - 70);

      }

      else if(state == 3) {

          g.setColor(0x00FFFFFF);
          g.fillRect(0, 0, getWidth(), getHeight());

          g.setColor(0x00AAAADD);
          for(l = -1; l < 2; l++) {
            for(m = -1; m < 2; m++) {
              g.drawString("Wool Mark", getWidth() / 2 + l, 10 + m, Graphics.HCENTER | Graphics.TOP);
            }
          }
          g.setColor(0x00CCCCFF);
          g.drawString("Wool Mark", getWidth() / 2, 10, Graphics.HCENTER | Graphics.TOP);

          g.setColor(0x0055CC55);
          g.drawString("draw frame:" + frame_number + "(times)", getWidth() / 2, 35, Graphics.HCENTER | Graphics.TOP);
          g.drawString("draw time:" + draw_time + "(ms)", getWidth() / 2, 50, Graphics.HCENTER | Graphics.TOP);

          g.setColor(0x00CC5555);
          g.drawString(getComaKazu(frame_number, draw_time) + "(times/sec)", getWidth() / 2, 70, Graphics.HCENTER | Graphics.TOP);

      }

  }

  private String getComaKazu(int frame, int time) {

    String temp = "0000";
    if(draw_time > 0) {
      temp = "" + ((1000 * frame_number) / (draw_time / 1000));
    }

    return temp.substring(0, temp.length() - 3) + "." + temp.substring(temp.length() - 3, temp.length());

  }

  public void keyPressed(int param) {
    if(param != 0) {
      key_flag |= 1 << getKeyNum(param);
    }
  }

  public void keyReleased(int param) {
    key_flag ^= 1 << getKeyNum(param);
  }

  private boolean isKeyPressed(int key) {
      return (key_flag & 1 << key) != 0;
  }

  private int getKeyNum(int keyin) {

    if(keyin == KEY_NUM0) {
      return KEY_0;
    } else if(keyin == KEY_NUM1) {
      return KEY_1;
    } else if(keyin == KEY_NUM2) {
      return KEY_2;
    } else if(keyin == KEY_NUM3) {
      return KEY_3;
    } else if(keyin == KEY_NUM4) {
      return KEY_4;
    } else if(keyin == KEY_NUM5) {
      return KEY_5;
    } else if(keyin == KEY_NUM6) {
      return KEY_6;
    } else if(keyin == KEY_NUM7) {
      return KEY_7;
    } else if(keyin == KEY_NUM8) {
      return KEY_8;
    } else if(keyin == KEY_NUM9) {
      return KEY_9;
    } else if(keyin == KEY_STAR) {
      return KEY_00;
    } else if(keyin == KEY_POUND) {
      return KEY_01;
    } else if(keyin == KEY_SOFT1) {
      return KEY_SOFT1;
    } else if(keyin == KEY_SOFT2) {
      return KEY_SOFT2;
    }

    else if(keyin < 0) {
      if(keyin == getKeyCode(UP)) {
        return KEY_UP;
      } else if(keyin == getKeyCode(RIGHT)) {
        return KEY_RIGHT;
      } else if(keyin == getKeyCode(DOWN)) {
        return KEY_DOWN;
      } else if(keyin == getKeyCode(LEFT)) {
        return KEY_LEFT;
      } else if(keyin == getKeyCode(FIRE)) {
        return KEY_SELECT;
      }
    }

    return 31;

  }

}

