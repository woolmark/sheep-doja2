import java.io.InputStream;
import java.util.Vector;

import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

import com.vodafone.v10.graphics.sprite.SpriteCanvas;

public abstract class VSCanvas extends SpriteCanvas{

    private Vector draw_sprite_stack;

    public static final int SPRITE_LOAD = -1;

    protected int state;

    protected int loading_cnt;

    protected int max_loading_cnt;

    private Vector vec;

    protected Sprite background;

    public VSCanvas(int numPalettes, int numPatterns, int height, int width) {
	super(numPalettes, numPatterns);
	createFrameBuffer(height, width);
	draw_sprite_stack = new Vector();
	vec = new Vector();
    }

    public void flush(int tx, int ty) {
	copyArea(0, 0, 128, 111, 0, 0);
	writeAllSprite();
	drawFrameBuffer(tx, ty);
    }

    public void clearSprite(){
	Sprite.totalIndex = 0;
    }

    public void drawSprite(Sprite sprite, int x, int y) {
	
	Sprite tmp = sprite.clone();
	
	tmp.x = x;
	tmp.y = y;
	tmp.turn = 0;
	
	draw_sprite_stack.addElement(tmp);
	
    }

    public void writeAllSprite() {

	for(int i=0;i<draw_sprite_stack.size();i++) {
	    Sprite sprite = (Sprite)draw_sprite_stack.elementAt(i);
	    writeSprite(sprite);
	}
	
	draw_sprite_stack.removeAllElements();
	background = null;
    }

    private void writeSprite(Sprite sprite) {
	try {

	    int index = sprite.index;
	    
	    int x = sprite.x;
	    int y = sprite.y;
	    
	    int wid   = sprite.tileX;
	    int hei   = sprite.tileY;
	    
	    int turn  = sprite.turn;

	    for(int j=0;j<hei;j++) {
		for(int i=0;i<wid;i++) {
		    
		    short px = (short)x;
		    short py = (short)y;
		    
		    px += (short)(i*8);
		    
		    py += (short)(j*8);
		    
		    drawSpriteChar(createCharacterCommand(0, true, 0, false, false, index+j*wid+i), px, py);
		}
	    }
	    
	}catch(Exception e) {
	}
	
    }

    public int getLoadingCnt(){
	return loading_cnt;
    }

    public int getMaxLoadingSprite(){
	return max_loading_cnt;
    }

    public Sprite[] setSprite(String NAME) throws ArrayIndexOutOfBoundsException {
	return setSprite(NAME, 0);
    }

    public Sprite[] setSprite(String NAME, int num) throws ArrayIndexOutOfBoundsException {
	
	InputStream in = getClass().getResourceAsStream(NAME);
	
	return setSprite(in, num);
	
    }

    public Sprite[] setSprite(InputStream in, int num) throws ArrayIndexOutOfBoundsException {
	
	Sprite[] spr = null;
	
	try {
	    
	    Sprite.totalIndex = num;
	    
	    state = SPRITE_LOAD;

	    int size = in.read() & 0x000000ff;
	    spr = new Sprite[size];
	    
	    loading_cnt = 0;
	    
	    max_loading_cnt = size;
	    
	    
	    for(int cnt=0;cnt<size;cnt++){
		
		int len = (in.read() << 8) & 0x0000ff00;
		len |= in.read() & 0x000000ff;
		
		byte[] buf = new byte[len+2];
		in.read(buf, 0, buf.length);
		
		spr[cnt] = addSprite(buf);
		
	    }
	    
	    in.close();
	}catch(Exception e){
	    throw new ArrayIndexOutOfBoundsException("スプライト数が256を越えました。");
	}
	
	return spr;
    }

    public Sprite addSprite(byte[] buf){
	
	int wid = (int)(buf[0] & 0x0000ff);
	int hei = (int)(buf[1] & 0x0000ff);
	
	byte[] tmp_data = new byte[buf.length-2];
	for(int i=0;i<tmp_data.length;i++){
	    tmp_data[i] = buf[i+2];
	}
	
	byte[][][] sprite = byteArrayToSprite(tmp_data, wid, hei);
	
	for(int i=0;i<sprite.length;i++) {
	    for(int j=0;j<sprite[0].length;j++) {
		setPattern(Sprite.totalIndex+i*sprite[0].length+j, sprite[i][j]);
	    }
	}
	
	Sprite spr = new Sprite();
	
	spr.index = Sprite.totalIndex;
	spr.tileX = (wid+7)/8;
	spr.tileY = (hei+7)/8;
	spr.width = wid;
	spr.height = hei;
	
	Sprite.totalIndex += ((wid+7)/8)*((hei+7)/8);
	
	
	return spr;
	
    }

    public void setPalette(String NAME) throws ArrayIndexOutOfBoundsException {
	try {
	    
	    InputStream in = getClass().getResourceAsStream(NAME);
	    
	    int size = in.read() & 0x000000ff;
	    
	    for(int i=0;i<size;i++) {
		
		
		int len = in.read() & 0x000000ff;
		
		byte[] tmp_data = new byte[len];
		in.read(tmp_data, 0, tmp_data.length);
		
		for(int j=0;j<tmp_data.length;j+=3) {
		    
		    int t = 0;
		    
		    t |= ((tmp_data[j]<<16) & 0x00ff0000);
		    t |= ((tmp_data[j+1]<<8) & 0x0000ff00);
		    t |= (tmp_data[j+2] & 0x000000ff);
		    
		    setPalette(i*32+j/3, t);
		}
	    }
	    
	    in.close();
	    
	}catch(Exception e){
	    throw new ArrayIndexOutOfBoundsException("パレット数が256を越えました。");
	}
	
    }

    private byte[][][] byteArrayToSprite(byte[] tmp_data, int wid, int hei) {
	
	int spwid = (wid+7)/8;
	int sphei = (hei+7)/8;
	
	byte[][][] sprite = new byte[sphei][spwid][64];
	try {
	    int place = 0;
	    for(int y=0;y<sphei;y++) {
		for(int x=0;x<spwid;x++) {
		    for(int i=0;i<64;i++) {
			sprite[y][x][i] = tmp_data[place++];
		    }
		}
	    }
	}catch(Exception e){}
	
	
	return sprite;
    }

    public void add(Object obj){
	vec.addElement(obj);
    }

    public void add(Object obj, int index){
	vec.insertElementAt(obj ,index);
    }

    public void remove(int index){
	vec.removeElementAt(index);
    }

    public void removeAll(){
	vec.removeAllElements();
    }

    public int getLength(){
	return vec.size();
    }

    public void draw(Graphics g, int index, int x, int y){
	try {
	    draw(g, vec.elementAt(index), x, y);
	}catch(ArrayIndexOutOfBoundsException e){}
    }

    public void draw(Graphics g, Object obj, int x, int y){
	
	if(obj instanceof Sprite){
	    drawSprite((Sprite)obj, x, y);
	}
	else if(obj instanceof Image){
	    g.drawImage((Image)obj, x, y, g.TOP|g.LEFT);
	}
	
    }
    
}


